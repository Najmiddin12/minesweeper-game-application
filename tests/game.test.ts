import { Game } from "../game";
import { GameState } from "../types";

test("Game initialization", () => {
  const game = new Game(10, 10, 5);
  expect(game.getGrid().length).toBe(10);
  expect(game.getGrid()[0].length).toBe(10);
});

test("Reveal cell", () => {
  const game = new Game(10, 10, 5);
  game.revealCell(0, 0);
  expect(game.getGrid()[0][0].isRevealed).toBe(true);
});

test("Flag cell", () => {
  const game = new Game(10, 10, 5);
  game.flagCell(0, 0);
  expect(game.getGrid()[0][0].isFlagged).toBe(true);
  game.flagCell(0, 0);
  expect(game.getGrid()[0][0].isFlagged).toBe(false);
});

test("Game over when mine is revealed", () => {
  const game = new Game(10, 10, 5);
  game.getGrid()[0][0].isMine = true;
  game.revealCell(0, 0);
  expect(game.getGameState()).toBe(GameState.Lost);
});
