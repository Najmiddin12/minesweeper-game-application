import { Game } from "./game";
import { GameState } from "./types";

document.addEventListener("DOMContentLoaded", () => {
  const game = new Game(10, 10, 5);

  const createBoard = () => {
    const board = document.getElementById("board");
    board.innerHTML = "";
    const grid = game.getGrid();

    for (let row = 0; row < grid.length; row++) {
      const rowDiv = document.createElement("div");
      rowDiv.className = "row";
      for (let col = 0; col < grid[row].length; col++) {
        const cellDiv = document.createElement("div");
        cellDiv.className = "cell";
        cellDiv.id = `cell-${row}-${col}`;
        cellDiv.addEventListener("click", () => handleCellClick(row, col));
        cellDiv.addEventListener("contextmenu", (e) => {
          e.preventDefault();
          handleCellFlag(row, col);
        });
        rowDiv.appendChild(cellDiv);
      }
      board.appendChild(rowDiv);
    }
  };

  const updateBoard = () => {
    const grid = game.getGrid();
    for (let row = 0; row < grid.length; row++) {
      for (let col = 0; col < grid[row].length; col++) {
        const cellDiv = document.getElementById(`cell-${row}-${col}`);
        const cell = grid[row][col];
        if (cell.isRevealed) {
          cellDiv.classList.add("revealed");
          cellDiv.textContent = cell.isMine ? "💣" : cell.adjacentMineCount.toString();
        } else if (cell.isFlagged) {
          cellDiv.textContent = "🚩";
        } else {
          cellDiv.textContent = "";
        }
      }
    }
  };

  const handleCellClick = (row: number, col: number) => {
    game.revealCell(row, col);
    updateBoard();
    checkGameState();
  };

  const handleCellFlag = (row: number, col: number) => {
    game.flagCell(row, col);
    updateBoard();
  };

  const checkGameState = () => {
    if (game.getGameState() === GameState.Won) {
      alert("Congratulations! You've won!");
    } else if (game.getGameState() === GameState.Lost) {
      alert("Game over! You've lost.");
    }
  };

  document.getElementById("restart").addEventListener("click", () => {
    location.reload();
  });

  createBoard();
  updateBoard();
});
