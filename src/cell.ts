import { Cell as CellInterface } from "./types";

export class Cell implements CellInterface {
  isMine: boolean;
  isRevealed: boolean;
  isFlagged: boolean;
  adjacentMineCount: number;

  constructor() {
    this.isMine = false;
    this.isRevealed = false;
    this.isFlagged = false;
    this.adjacentMineCount = 0;
  }
}
