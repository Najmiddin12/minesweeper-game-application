export enum GameState {
    InProgress,
    Won,
    Lost
}
  
export interface Cell {
    isMine: boolean;
    isRevealed: boolean;
    isFlagged: boolean;
    adjacentMineCount: number;
}  