import { GameState, Cell as CellInterface } from "./types";
import { Cell } from "./cell";

export class Game {
  private grid: Cell[][];
  private gameState: GameState;
  private rows: number;
  private cols: number;
  private mines: number;

  constructor(rows: number, cols: number, mines: number) {
    this.rows = rows;
    this.cols = cols;
    this.mines = mines;
    this.grid = this.createGrid();
    this.gameState = GameState.InProgress;
    this.placeMines();
    this.calculateAdjacentMines();
  }

  private createGrid(): Cell[][] {
    return Array.from({ length: this.rows }, () =>
      Array.from({ length: this.cols }, () => new Cell())
    );
  }

  private placeMines(): void {
    let placedMines = 0;
    while (placedMines < this.mines) {
      const row = Math.floor(Math.random() * this.rows);
      const col = Math.floor(Math.random() * this.cols);
      if (!this.grid[row][col].isMine) {
        this.grid[row][col].isMine = true;
        placedMines++;
      }
    }
  }

  private calculateAdjacentMines(): void {
    const directions = [
      [-1, -1], [-1, 0], [-1, 1],
      [0, -1],          [0, 1],
      [1, -1], [1, 0], [1, 1]
    ];

    for (let row = 0; row < this.rows; row++) {
      for (let col = 0; col < this.cols; col++) {
        if (this.grid[row][col].isMine) continue;
        let mineCount = 0;
        for (const [dx, dy] of directions) {
          const newRow = row + dx;
          const newCol = col + dy;
          if (newRow >= 0 && newRow < this.rows && newCol >= 0 && newCol < this.cols && this.grid[newRow][newCol].isMine) {
            mineCount++;
          }
        }
        this.grid[row][col].adjacentMineCount = mineCount;
      }
    }
  }

  public revealCell(row: number, col: number): void {
    if (this.gameState !== GameState.InProgress || this.grid[row][col].isRevealed || this.grid[row][col].isFlagged) {
      return;
    }

    this.grid[row][col].isRevealed = true;
    if (this.grid[row][col].isMine) {
      this.gameState = GameState.Lost;
      this.revealAllMines();
    } else if (this.grid[row][col].adjacentMineCount === 0) {
      this.revealAdjacentCells(row, col);
    }

    if (this.checkWinCondition()) {
      this.gameState = GameState.Won;
    }
  }

  private revealAllMines(): void {
    for (const row of this.grid) {
      for (const cell of row) {
        if (cell.isMine) {
          cell.isRevealed = true;
        }
      }
    }
  }

  private revealAdjacentCells(row: number, col: number): void {
    const directions = [
      [-1, -1], [-1, 0], [-1, 1],
      [0, -1],          [0, 1],
      [1, -1], [1, 0], [1, 1]
    ];

    for (const [dx, dy] of directions) {
      const newRow = row + dx;
      const newCol = col + dy;
      if (newRow >= 0 && newRow < this.rows && newCol >= 0 && newCol < this.cols && !this.grid[newRow][newCol].isRevealed) {
        this.revealCell(newRow, newCol);
      }
    }
  }

  public flagCell(row: number, col: number): void {
    if (this.gameState !== GameState.InProgress || this.grid[row][col].isRevealed) {
      return;
    }
    this.grid[row][col].isFlagged = !this.grid[row][col].isFlagged;
  }

  private checkWinCondition(): boolean {
    for (const row of this.grid) {
      for (const cell of row) {
        if (!cell.isMine && !cell.isRevealed) {
          return false;
        }
      }
    }
    return true;
  }

  public getGameState(): GameState {
    return this.gameState;
  }

  public getGrid(): Cell[][] {
    return this.grid;
  }
}
